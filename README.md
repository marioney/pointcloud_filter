# README #

ROS package for downsampling a pointcloud2 message.

Based on the  [pcl_decimator package](https://github.com/bk8190/bk-ros/tree/master/pcl_decimator)  created by Bill Kulp

Tested on ROS Indigo.