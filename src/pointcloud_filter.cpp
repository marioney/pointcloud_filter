#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <pointcloud_filter/PointCloudFilterConfig.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include "pcl/io/pcd_io.h"
#include "pcl/filters/passthrough.h"
#include "pcl/filters/voxel_grid.h"
#include "pcl/filters/radius_outlier_removal.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>


#include <string>
#include <tf/transform_listener.h>
#include <sensor_msgs/point_cloud_conversion.h>

ros::Publisher cloudout_pub_, cloudout_old_pub_;

std::string fieldName_ = "z";
float startPoint_ = .4;
float endPoint_ = 1.0;
float leafSize_ = 0.02;
float outlierRadius_ = 0.2;
int   numInRadius_ = 5;

tf::TransformListener* tfl;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;

// CALLBACKS
void pointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr& cloud_raw)
{
    sensor_msgs::PointCloud2::Ptr cloud_transformed (new sensor_msgs::PointCloud2());
    sensor_msgs::PointCloud2 cloud_inliers;
    //sensor_msgs::PointCloud2::Ptr cloud_downsampled (new sensor_msgs::PointCloud2());

    // Transform the pointcloud
//    try{
//        ROS_DEBUG_STREAM("Transforming");
//        pcl_ros::transformPointCloud( "base_link", *cloud_raw, *cloud_transformed, *tfl);
//    } catch( tf::TransformException ex ){
//        ROS_WARN_STREAM("PCL decimator could not transform: " << ex.what() );
//        return;
//    }

    ros::Time t1 = ros::Time::now();

    // ROS_INFO("Filtering");
    // Create a pass-through filter
    pcl::PCLPointCloud2 pcl_pc2;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr inliers_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    ROS_INFO("PCL");
    // Transform to PCL
    pcl_conversions::toPCL(*cloud_raw,pcl_pc2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2(pcl_pc2,*temp_cloud);

    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud      (temp_cloud );
    pass.setFilterFieldName (fieldName_        );
    pass.setFilterLimits    (startPoint_, endPoint_);
    pass.filter             (*cropped_cloud    );

    ROS_INFO("Cropped");
    // Now create a voxel grid filter
    pcl::VoxelGrid<pcl::PointXYZ> vgrid;
    vgrid.setInputCloud     (cropped_cloud      );
    vgrid.setLeafSize       (leafSize_, leafSize_, leafSize_);
    vgrid.filter            (*downsampled_cloud );

    ROS_INFO("downsampled");
    // Remove outliers
    pcl::RadiusOutlierRemoval<pcl::PointXYZ> radius;
    radius.setInputCloud    (downsampled_cloud     );
    radius.setRadiusSearch  (outlierRadius_    );
    radius.setMinNeighborsInRadius(numInRadius_);
    radius.filter           (*inliers_cloud    );

    ROS_INFO("back to ros");

    // Transform back to ros msg pointcloud2
    pcl::toROSMsg(*inliers_cloud, cloud_inliers);


    ros::Duration d = ros::Time::now() - t1;
    ROS_DEBUG_STREAM("Filtering took " << (d.toSec()) << " seconds.");

    cloud_inliers.header = cloud_raw->header;
    cloudout_pub_.publish   (cloud_inliers);

    // Convert to
//    sensor_msgs::PointCloud old_cloud;
//    sensor_msgs::convertPointCloud2ToPointCloud(*cloud_downsampled, old_cloud);
//    cloudout_old_pub_.publish(old_cloud);


    ros::Duration(0.5).sleep();
}



void callback(pointcloud_filter::PointCloudFilterConfig &config, uint32_t level)
{
  fieldName_  = config.field_name;
  startPoint_ = config.start_threshold;
  endPoint_   = config.end_threshold;
  leafSize_   = config.leaf_size;
  numInRadius_= config.num_in_radius;
  outlierRadius_ = config.outlier_radius;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "pcl_decimator");
  ros::NodeHandle n;
  tfl = new tf::TransformListener();

  // Allow the transform listner some time to receive messages
  ros::Duration(3).sleep();

  cloudout_pub_     = n.advertise< sensor_msgs::PointCloud2 >("/camera/depth/points_l"    , 1);
  //cloudout_old_pub_ = n.advertise< sensor_msgs::PointCloud  >("/camera/depth/points_sliced_old", 1);

  ros::Subscriber kinect_sub = n.subscribe("/camera/depth/points", 1, pointcloudCallback);

  dynamic_reconfigure::Server<pointcloud_filter::PointCloudFilterConfig> srv;
  dynamic_reconfigure::Server<pointcloud_filter::PointCloudFilterConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  srv.setCallback(f);

  ros::spin();

  delete tfl;
  return 0;
}
