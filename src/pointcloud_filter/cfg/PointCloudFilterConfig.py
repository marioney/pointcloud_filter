## *********************************************************
## 
## File autogenerated for the pointcloud_filter package 
## by the dynamic_reconfigure package.
## Please do not edit.
## 
## ********************************************************/

from dynamic_reconfigure.encoding import extract_params

inf = float('inf')

config_description = {'upper': 'DEFAULT', 'lower': 'groups', 'srcline': 233, 'name': 'Default', 'parent': 0, 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'cstate': 'true', 'parentname': 'Default', 'class': 'DEFAULT', 'field': 'default', 'state': True, 'parentclass': '', 'groups': [], 'parameters': [{'srcline': 259, 'description': 'The axis on which to perform the multiple passthroughs', 'max': '', 'cconsttype': 'const char * const', 'ctype': 'std::string', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'field_name', 'edit_method': '', 'default': 'z', 'level': 1, 'min': '', 'type': 'str'}, {'srcline': 259, 'description': 'The measurement at which to place the first slice.', 'max': 100.0, 'cconsttype': 'const double', 'ctype': 'double', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'start_threshold', 'edit_method': '', 'default': 0.2, 'level': 1, 'min': -100.0, 'type': 'double'}, {'srcline': 259, 'description': 'The measurement at which to place the final slice.', 'max': 100.0, 'cconsttype': 'const double', 'ctype': 'double', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'end_threshold', 'edit_method': '', 'default': 1.0, 'level': 1, 'min': -100.0, 'type': 'double'}, {'srcline': 259, 'description': 'Resolution of the voxel grid filter.', 'max': 1.0, 'cconsttype': 'const double', 'ctype': 'double', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'leaf_size', 'edit_method': '', 'default': 0.1, 'level': 1, 'min': 0.005, 'type': 'double'}, {'srcline': 259, 'description': 'Required number of inliers.', 'max': 50, 'cconsttype': 'const int', 'ctype': 'int', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'num_in_radius', 'edit_method': '', 'default': 2, 'level': 1, 'min': 0, 'type': 'int'}, {'srcline': 259, 'description': 'Resolution of the inlier filter.', 'max': 1.0, 'cconsttype': 'const double', 'ctype': 'double', 'srcfile': '/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py', 'name': 'outlier_radius', 'edit_method': '', 'default': 0.2, 'level': 1, 'min': 0.005, 'type': 'double'}], 'type': '', 'id': 0}

min = {}
max = {}
defaults = {}
level = {}
type = {}
all_level = 0

#def extract_params(config):
#    params = []
#    params.extend(config['parameters'])    
#    for group in config['groups']:
#        params.extend(extract_params(group))
#    return params

for param in extract_params(config_description):
    min[param['name']] = param['min']
    max[param['name']] = param['max']
    defaults[param['name']] = param['default']
    level[param['name']] = param['level']
    type[param['name']] = param['type']
    all_level = all_level | param['level']

